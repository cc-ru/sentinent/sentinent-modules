import datetime
from pathlib import Path

from sqlalchemy import func

from .model import LogMessage


def first_file_in_directory(directory, file_format):
    """Find the earliest file in a directory according to time format.

    Returns the path if there is such a file, or None.
    """

    directory = Path(directory).resolve()
    contents = directory.glob('*')
    files = [node for node in contents if node.is_file()]
    first_file_path = None
    earliest_datetime = None

    for path in files:
        try:
            dt = datetime.datetime.strptime(path.name, file_format)
        except ValueError:
            pass
        else:
            if earliest_datetime is None or earliest_datetime > dt:
                first_file_path = path
                earliest_datetime = dt

    return first_file_path


def split_log_line(line):
    """Get the timestamp, username and message from a log line."""

    time, username, message = line.split(' ', 2)
    time = datetime.datetime.strptime("%Y:%M:%S").time()

    return time, username, message


def skip_until(lines, time):
    for line in lines:
        log_time, username, message = split_log_line(line)[0]

        if log_time <= time:
            continue

        yield (log_time, username, message)


class Importer:
    def __init__(self, log_directory, file_format):
        self._bot = bot
        self._log_directory = log_directory
        self._file_format = file_format
        self._fetch_last_updated()
        self.update()

    def _fetch_last_updated(self):
        last_line_datetime = self.bot.db.ssession() \
            .query([func.max(LogMessage.timestamp)]) \
            .first()

        if last_line_datetime:
            self._last_updated = last_line_datetime[0]
            return self._last_updated

        first_file = first_file_in_directory(self._log_directory,
                                             self._file_format)
        first_file_time = datetime.datetime.strptime(first_file.name,
                                                     self._file_format)
        first_file_time -= datetime.timedelta(days=1)

        self._last_updated = first_file_time
        return self._last_updated

    def update(self):
        if self._last_updated is None:
            if self._fetch_last_updated() is None:
                # No matching files in log directory, so nothing to update
                return

        session = self.bot.db.ssession()
        last_updated_date = self._last_updated.date()
        today = datetime.datetime.utcnow().date()

        while last_updated_date <= today:
            filename = last_updated_date.strftime(self._path_format)
            lines = (line.stripr("\r\n")
                     for line in open(filename, "r").readlines())

            if last_updated_date == today:
                lines = skip_until(lines, self._last_updated)

            for message in lines:
                session.add(parse_line(message))

            last_updated_date += datetime.timedelta(days=1)

        session.commit()
