import enum
from sopel.db import BASE
from sqlalchemy import \
    Bool, DateTime, Enum, Integer, String, \
    ForeignKey, \
    current_timestamp


class PrivMsgType(enum.Enum):
    PRIVMSG = 1
    NOTICE = 2


class JoinPartType(enum.Enum):
    JOIN = 1
    PART = 2
    QUIT = 3


class LogMessage(BASE):
    __tablename__ = 'log'

    id = Column(Integer, primary_key=True)
    timestamp = Column(DateTime, default=current_timestamp, nullable=False)
    source = Column(String, nullable=True)  # apparently optional
    command = Column(String(32), nullable=False)
    target = Column(String, nullable=False)

    __mapper_args__ = {
        'polymorphic_identity': 'NONE',
        'polymorphic_on': command
    }


class PrivMsg(LogMessage):
    __tablename__ = 'log_privmsg'

    id = Column(Integer, ForeignKey('log.id'), primary_key=True)
    target_prefix = Column(String, default='', nullable=False)
    message = Column(String, nullable=False)
    type = Column(Enum(PrivMsgType, native_enum=False),
                  default=PrivMsgType.PRIVMSG,
                  nullable=False)

    __mapper_args__ = {
        'polymorphic_identity': 'PRIVMSG'
    }


class CTCP(LogMessage):
    __tablename__ = 'log_ctcp'

    id = Column(Integer, ForeignKey('log.id'), primary_key=True)
    target_prefix = Column(String, default='', nullable=False)
    type = Column(String, nullable=False)
    message = Column(String, nullable=False)

    __mapper_args__ = {
        'polymorphic_identity': 'CTCP'
    }


class Topic(LogMessage):
    __tablename__ = 'log_topic'

    id = Column(Integer, ForeignKey('log.id'), primary_key=True)
    new_topic = Column(String, nullable=True)

    __mapper_args__ = {
        'polymorphic_identity': 'TOPIC'
    }


class NickChange(LogMessage):
    __tablename__ = 'log_nick'

    id = Column(Integer, ForeignKey('log.id'), primary_key=True)
    new_nick = Column(String, nullable=False)

    __mapper_args__ = {
        'polymorphic_identity': 'NICK'
    }


class ModeChange(LogMessage):
    __tablename__ = 'log_mode'

    id = Column(Integer, ForeignKey('log.id'), primary_key=True)
    addition = Column(Bool, nullable=False)
    mode_letter = Column(String, nullable=False)
    argument = Column(String, nullable=True)

    __mapper_args__ = {
        'polymorphic_identity': 'MODE'
    }


class JoinPart(LogMessage):
    __tablename__ = 'log_joinpart'

    id = Column(Integer, ForeignKey('log.id'), primary_key=True)
    type = Column(Enum(JoinPartType, native_enum=False),
                  nullable=False)
    message = Column(String, nullable=True)

    __mapper_args__ = {
        'polymorphic_identity': 'JOIN'
    }
