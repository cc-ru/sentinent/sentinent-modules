from sopel import module
from sopel.config.types import StaticSection, BaseValidated
import socket
import dns.resolver
from mcstatus import MinecraftServer
from mcstatus.pinger import ServerPinger
from mcstatus.protocol.connection import TCPSocketConnection, \
    UDPSocketConnection
from mcstatus.querier import ServerQuerier


class MapAttribute(BaseValidated):
    ESCAPE_CHARACTER = '\\'
    DELIMITER = ','
    ASSIGNMENT = '='

    def __init__(self, name, default={}):
        super().__init__(name, default=default)

    def parse(self, value):
        result = {}
        key = None
        escaped = False
        token = ''

        def insert():
            key = key.strip()
            token = token.strip()

            if key:
                if key in result:
                    raise ValueError('Duplicate key {}'.format(key))
                result[key] = token
                token = ''
                key = None
            else:
                raise ValueError('Value not specified '
                                 'for key {}'.format(token))

        for char in value:
            if escaped:
                token += char
                escaped = False
            else:
                if char == self.ESCAPE_CHARACTER:
                    escaped = True
                elif char == self.DELIMITER:
                    insert()
                elif not key and token and char == self.ASSIGNMENT:
                    key = token
                    token = ''

        if key or token:
            insert()

        return result

    def serialize(self, value):
        if not isinstance(value, dict):
            raise ValueError('MapAttribute value must be a dict')

        def escape(s):
            result = []

            for char in s.strip():
                if char in [self.ESCAPE_CHARACTER, self.DELIMITER,
                            self.ASSIGNMENT]:
                    result.append(self.ESCAPE_CHARACTER)
                result.append(s)

            return ''.join(s)

        return ', '.join(['{} = {}'.format(escape(k), escape(v))
                          for k, v in value.items()])


class McSection(StaticSection):
    servers = MapAttribute('servers', default=[])


class FixedTCPSocketConnection(TCPSocketConnection):
    def __del__(self):
        if hasattr(self, 'socket') and self.socket:
            super().__del__()


class FixedUDPSocketConnection(UDPSocketConnection):
    def __del__(self):
        if hasattr(self, 'socket') and self.socket:
            super().__del__()


class MinecraftServerWithTimeout(MinecraftServer):
    def __init__(self, host, port=25565, timeout=3):
        super().__init__(host, port)
        self.host = host
        self.port = port
        self._timeout = timeout

    @staticmethod
    def lookup(address):
        host = address
        port = None
        if ":" in address:
            parts = address.split(":")
            if len(parts) > 2:
                raise ValueError("Invalid address '%s'" % address)
            host = parts[0]
            port = int(parts[1])
        if port is None:
            port = 25565
            # noinspection PyBroadException
            try:
                answers = dns.resolver.query("_minecraft._tcp." + host, "SRV")
                if len(answers):
                    answer = answers[0]
                    host = str(answer.target).rstrip(".")
                    port = int(answer.port)
            except Exception:
                pass

        return MinecraftServerWithTimeout(host, port)

    @property
    def timeout(self):
        return self._timeout

    @timeout.setter
    def timeout(self, timeout):
        self._timeout = timeout

    def ping(self, retries=3, **kwargs):
        connection = FixedTCPSocketConnection((self.host, self.port),
                                              self.timeout)
        exception = None
        for attempt in range(retries):
            try:
                pinger = ServerPinger(connection, host=self.host,
                                      port=self.port, **kwargs)
                pinger.handshake()
                return pinger.test_ping()
            except Exception as e:
                exception = e
        else:
            raise exception

    def status(self, retries=3, **kwargs):
        connection = FixedTCPSocketConnection((self.host, self.port),
                                              self.timeout)
        exception = None
        for attempt in range(retries):
            try:
                pinger = ServerPinger(connection, host=self.host,
                                      port=self.port, **kwargs)
                pinger.handshake()
                result = pinger.read_status()
                result.latency = pinger.test_ping()
                return result
            except Exception as e:
                exception = e
        else:
            raise exception

    def query(self, retries=3):
        exception = None
        host = self.host
        try:
            answers = dns.resolver.query(host, "A")
            if len(answers):
                answer = answers[0]
                host = str(answer).rstrip(".")
        except:
            pass
        for attempt in range(retries):
            try:
                connection = FixedUDPSocketConnection((host, self.port),
                                                      self.timeout)
                querier = ServerQuerier(connection)
                querier.handshake()
                return querier.read_query()
            except Exception as e:
                exception = e
        else:
            raise exception


class ServerData:
    def __init__(self, server: MinecraftServerWithTimeout):
        self.server = server
        self._status = None
        self._query = None

    @property
    def status(self):
        if self._status is None:
            try:
                self._status = self.server.status(retries=1)
            except (socket.gaierror, socket.timeout, ConnectionError, OSError):
                self._status = False
        return self._status

    @property
    def query(self):
        if self._query is None:
            try:
                self._query = self.server.query(retries=1)
            except (socket.gaierror, socket.timeout, ConnectionError, OSError):
                self._query = False
        return self._query

    @property
    def on(self):
        if self.status or self.query:
            return True
        else:
            return False

    @property
    def latency(self):
        if self.status:
            return self.status.latency
        else:
            return None

    @property
    def online(self):
        if self.status:
            return self.status.players.online
        elif self.query:
            return self.query.players.online
        else:
            return None

    @property
    def max(self):
        if self.status:
            return self.status.players.max
        elif self.query:
            return self.query.players.max
        else:
            return None

    @property
    def players(self):
        if self.online is not None:
            if self.online == 0:
                return []
            else:
                if self.status and self.status.players.sample:
                    return [player.name for player in
                            self.status.players.sample]
                elif self.query:
                    return [player for player in self.query.players.names]
                else:
                    return None
        else:
            return None

    @property
    def software(self):
        if self.status:
            return self.status.version.name
        elif self.query:
            return self.query.software.version
        else:
            return None

    @property
    def mods(self):
        if self.status and 'modinfo' in self.status.raw:
            return self.status.raw['modinfo']['modList']
        elif self.query and self.query.software.plugins:
            return self.query.software.plugins
        else:
            return None


def get_server_data(address):
    server = MinecraftServerWithTimeout.lookup(address)
    server.timeout = 0.5
    server_data = ServerData(server)
    return server_data


def configure(config):
    config.define_section('mc', McSection, validate=False)
    config.mc.configure_setting('servers',
                                'Mappings of names to addresses '
                                '(e.g. a=127.0.0.1:25565, b=localhost:26675)')


def setup(bot):
    bot.config.define_section('mc', McSection)


@module.commands("mc")
@module.example(".mc", ".mc RC")
def mc(bot, trigger):
    """Get server statuses."""
    if not trigger.group(2):
        out = []

        for name, info in bot.config.mc.servers.items():
            data = get_server_data(info["address"])
            if not data.on:
                chunk = "\x0304{}\x0f: OFF".format(name)
            else:
                chunk = "\x0303{}\x0f".format(name)
                if data.online is not None:
                    chunk += ": {}\x02/\x02{}".format(data.online, data.max)
            out.append(chunk)

        bot.reply(" \x0315:\x0f ".join(out))
    else:
        address = trigger.group(2).strip()
        serv = [v for k, v in bot.config.mc.servers.items()
                if k.lower() == address.lower()]
        if len(serv) == 1:
            address = serv[0]["address"]

        hostname = address
        port = 25565
        if hostname.find(":") != -1:
            (hostname, port) = hostname.split(":", 1)
            try:
                port = int(port)
            except ValueError:
                pass

        data = get_server_data(address)

        if not data.on:
            out = "\x02[\x02\x0304{}\x0f\x02]\x02 OFF".format(address)
        else:
            out = "\x02[\x02\x0303{}\x0f\x02]\x02".format(address)
            if data.online is not None:
                out += " {}\x02/\x02{}".format(data.online, data.max)

            if data.software:
                out += " \x02[\x02{}\x02]\x02".format(data.software)

            if data.players:
                out += "\x0315:\x0f " + "\x0315, \x0f".join(data.players)

        bot.reply(out)
